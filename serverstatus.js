window.onload = async() => {
    const IP = "YOUR-SERVERIP";
    const PORT = "YOUR-SERVERPORT";
    try {
        let req = await fetch("https://cdn.rage.mp/master/", {
            method: "GET",
        });
        let data = await req.json();
        let serverData = data[`${IP}:${PORT}`];
        if(!serverData) {
            document.getElementById("serverOnline").innerText = "offline";
            document.getElementById("serverPlayerAmount").innerText = "0";
        }
        else {
            let currentPlayers = serverData.players;
            /*let name = serverData.name;
            let gamemode = serverData.gamemode;
            let website = serverData.url;
            let peakPlayers = serverData.peak;
            let maxPlayers = serverData.maxplayers;*/
            document.getElementById("serverOnline").innerText = "online";
            document.getElementById("serverPlayerAmount").innerText = `${currentPlayers}`;
        }
    }
    catch(e) {
        console.error(e);
    }
};
